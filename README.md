# Image based heart sound classification

Dependencies (python packages used): numpy, spectrum, soundfile, keras, tensorflow, matplotlib, scipy, resampy, gammatone (https://github.com/detly/gammatone).  

Work directory tree  
.  
└─data    
├─────training-a  
├─────training-b  
├─────training-c  
├─────training-d  
├─────training-e  
└─────training-f  
└─(source code, README.md, .gitignore, etc)  