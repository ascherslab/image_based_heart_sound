import os
import urllib.request
import zipfile

#os.chdir(os.path.dirname(__file__))
localRepoDir=os.getcwd()
dataFolder = localRepoDir + "/data/"
featureFolder = dataFolder + "features/"
resultsFolder = dataFolder + "results/"
dbaName = 'Physionet'

"""
# Collect test file list together with size information
    # testDataFolder=dataFolder+'/test/'
    # testFileSizes={}
    # for root, dirs, files in os.walk(testDataFolder):
    #     for file in files:
    #         if file.endswith('.wav') or file.endswith('.WAV'):
    #             testFileSizes[file]=os.stat(os.path.join(root, file)).st_size
    #             
    # #Remove duplicates in the train folder checking file name and size
    # trainDataFolder=dataFolder+'/train/'
    # for root, dirs, files in os.walk(trainDataFolder):
    #     for file in files:
    #         if file in testFileSizes:
    #             fileSize=os.stat(os.path.join(root, file)).st_size
    #             if(fileSize == testFileSizes[file]):
    #                 #print('File to be deleted: ', file, ' size: ',fileSize)
    #                 os.remove(os.path.join(root, file))
    #                 #If exists, remove also the label file
    #                 labelFile=os.path.join(root, file.replace('.wav','.hea'))
    #                 if os.path.exists(labelFile):
    #                     os.remove(labelFile)
    # 
"""

# The results will be put in 'results' folder in the database folder
resultsFolder = dataFolder + "/results/"
if not os.path.exists(resultsFolder):  # create folder if not exists
    os.mkdir(resultsFolder)
# The features will be saved in 'features' folder in the database folder
featureFolder = dataFolder + "/features/"
if not os.path.exists(featureFolder):  # create folder if not exists
    os.mkdir(featureFolder)

#TEST specification
from Segmentation import Segmentation
from Feature import Feature
from Data import Data
from Test import Test
from miscFuncs import cleanFilesOfPrevSess


info="Single-test repeated experiment"#any info about the test
#Setting train-validation-test split ratios, 
# if 'test' exists as a static folder, the first two values are used 
# and rest of the data is split taking into account their ratio
splitRatios=[0.65,0.15,0.20]

#This flag defines if the same number of samples will be used for each class in training
#   If True, data augmentation(via up-sampling some existing files) will be carried
#   to have balanced set for training and validation. Not applicable to test files
useBalancedData=True

#Define segmentation strategy
async2secSegments=Segmentation("None",periodSync=False,sizeType="fixed",frameSizeMs=2000.0,hopSizeMs=1000.0)
# async3secSegments=Segmentation("None",periodSync=False,sizeType="fixed",frameSizeMs=3000.0,hopSizeMs=1000.0)
segStrategies=[async2secSegments]

#Define features to be used
features=[]
for featName in ['MelSpec']:#options: 'SubEnv','MFCC','MelSpec'
    for segType in segStrategies:
        for timeDim in [64, 32]:
            for freqDim in [16]:
                features.append(Feature(featName,[timeDim,freqDim],"frame",segType,window4frame='hanning'))


#Define data specifications for this database
data=Data(dbaName,dataFolder,featureFolder,features,useBalancedData,splitRatios,info)
#Defining NN model with a name. 
#   Implementation is in models.py. Feel free to add your own models and 
#   test by just changing the name here
modelNames=['Alex']
# modelNames=['uocSeq2','uocSeq1']

#Running random split and testing several times (1/testSetPercentage)
# ex: if test set is 20%, tests will be repeated 5 times
# numExperiments=int(1/splitRatios[-1])
# for i in range(numExperiments):
for modelName in modelNames:
    #Define test specifications and run
    singleTest=Test(modelName,data,resultsFolder,batch_size=128,num_epochs=50)
    #Run the tests: outputs will be put in the results folder
    singleTest.run()
    #Cleaning this test sessions' intermediate files
# cleanFilesOfPrevSess([dataFolder])
